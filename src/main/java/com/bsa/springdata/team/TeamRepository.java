package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

      Optional<Team> findByName(String name);

      @Modifying
      @Transactional
      @Query(nativeQuery = true, value = "update teams set name = " +
              "concat_ws('_', name, " +
                  "(select name from projects where id = teams.project_id), " +
                  "(select name from technologies where id = teams.technology_id)" +
              ") " +
              "where name = :hipsters")
      void normalizeName(String hipsters);

      @Modifying
      @Transactional
      @Query("update Team t set t.technology = :technology where size(t.users) < :devsNumber and t.technology = :oldTechnology")
      void updateTechnology(Optional<Integer> devsNumber, Technology oldTechnology, Technology technology);

      int countByTechnologyName(String technologyName);
}
