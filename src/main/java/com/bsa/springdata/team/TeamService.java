package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TeamService {
    private TeamRepository teamRepository;
    private TechnologyRepository technologyRepository;

    @Autowired
    public TeamService(TeamRepository teamRepository, TechnologyRepository technologyRepository) {
        this.teamRepository = teamRepository;
        this.technologyRepository = technologyRepository;
    }

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        var technology = technologyRepository.findByName(newTechnologyName)
                .orElseThrow();

        var oldTechnology = technologyRepository.findByName(oldTechnologyName)
                .orElseThrow();

        teamRepository.updateTechnology(Optional.of(devsNumber), oldTechnology, technology);
    }

    public void normalizeName(String hipsters) {
        teamRepository.normalizeName(hipsters);
    }

}
