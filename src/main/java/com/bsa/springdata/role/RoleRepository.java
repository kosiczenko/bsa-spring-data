package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = "delete from roles r where r.code = ?1 and r.id not in (select distinct u2r.role_id as id from User2role u2r)")
    void deleteRole(String roleCode);
}
