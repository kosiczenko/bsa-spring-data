package com.bsa.springdata.office;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OfficeService {

    private OfficeRepository officeRepository;

    @Autowired
    public OfficeService(OfficeRepository officeRepository) {
        this.officeRepository = officeRepository;
    }

    public List<OfficeDto> getByTechnology(String technology) {
        return officeRepository.getByTechnology(technology)
                .stream()
                .map(OfficeDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<OfficeDto> updateAddress(String oldAddress, String newAddress) {
        officeRepository.updateAddress(oldAddress, newAddress);

        var officeList = officeRepository
                .getByAddress(newAddress)
                .stream()
                .map(OfficeDto::fromEntity)
                .collect(Collectors.toList());

        if( officeList.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(officeList.get(0));
    }
}
