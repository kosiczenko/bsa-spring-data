package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query(nativeQuery=true, value = "select distinct o.* from offices o \n" +
            "left outer join users u on u.office_id = o.id \n" +
            "left outer join teams t on t.id = u.team_id \n" +
            "left outer join technologies ON technologies.id = t.technology_id \n" +
            "where technologies.name = ?1")
    List<Office> getByTechnology(String technology);

    List<Office> getByAddress(String adress);

    @Modifying
    @Transactional
    @Query(value = "update Office o set o.address = :address " +
            "where o.address = :oldAddress and (size(o.users) >0)")
    void updateAddress(String oldAddress, String address);

}
