package com.bsa.springdata.user;

import com.bsa.springdata.user.dto.UserDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    List<User> findByLastNameStartsWithIgnoreCaseOrderByLastNameAsc(String lastName, Pageable pageable);

    List<User> findByOfficeCityOrderByLastNameAsc(String city);

    List<User> findByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    @Query("select u from User u where u.office.city = ?1 and u.team.room = ?2 order by u.lastName asc")
    List<User> findByTeamRoomAndCity(String city, String room);

    @Transactional
    @Modifying
    int deleteByExperienceIsLessThan(int experience);
}
