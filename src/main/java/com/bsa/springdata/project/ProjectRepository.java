package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {
    @Query(nativeQuery = true,
       value = "select * from projects p " +
            "join (select t.project_id, count(*) as usersCount from teams t " +
            "join technologies t2 on t.technology_id = t2.id " +
            "join users u on u.team_id = t.id where t2.name = :technology " +
            "group by project_id) as res " +
            "on p.id = res.project_id order by usersCount DESC ")
    List<Project> findTop5ByTechnology(@Param("technology") String technology, Pageable pageable);

    @Query(nativeQuery = true,
            value = "select p.*, count(t.id) count_teams, count(u.id) count_users from projects p " +
                    "left outer join teams t on t.project_id = p.id " +
                    "left outer join users u on u.team_id = t.id " +
                    "group by p.id order by count_teams desc, count_users desc, p.name desc limit 1")
    List<Project> findTheBiggest();

    @Query(nativeQuery = true,
            value = "select distinct p.name," +
                    "count(distinct t.id) as teamsNumber, " +
                    "count(distinct u.id) as developersNumber, " +
                    "string_agg(distinct t2.name, ',' order by t2.name desc) as technologies " +
                    "from teams t " +
                    "join users u on t.id=u.team_id " +
                    "join technologies t2 on t2.id = t.technology_id " +
                    "join projects p on p.id = t.project_id " +
                    "group by p.name order by p.name")
    List<ProjectSummaryDto> getSummary();

    @Query(nativeQuery = true,
    value = "select count(inner_query.p)  from (select DISTINCT p from projects p \n" +
            "left outer join teams t on t.project_id = p.id \n" +
            "left outer join users u on u.team_id = t.id\n" +
            "left outer join user2role u2r on u2r.user_id = u.id  \n" +
            "left outer join roles r on r.id = u2r.role_id where r.name = ?1) as inner_query")
    int getCountWithRole(String role);

}