package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    private ProjectRepository projectRepository;
    private TechnologyRepository technologyRepository;
    private TeamRepository teamRepository;

    @Autowired
    public ProjectService(ProjectRepository projectRepository, TechnologyRepository technologyRepository, TeamRepository teamRepository) {
        this.projectRepository = projectRepository;
        this.technologyRepository = technologyRepository;
        this.teamRepository = teamRepository;
    }

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        return projectRepository.findTop5ByTechnology(technology, PageRequest.of(0,5))
                .stream()
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        return Optional.of(projectRepository.
                findTheBiggest().
                stream().
                map(ProjectDto::fromEntity).
                collect(Collectors.toList()).
                get(0));
    }

    public List<ProjectSummaryDto> getSummary() {
        return projectRepository.getSummary();
    }

    public int getCountWithRole(String role) {
         return projectRepository.getCountWithRole(role);
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        var  project = Project.builder()
                .name(createProjectRequest.getProjectName())
                .description(createProjectRequest.getProjectDescription())
                .teams(new ArrayList<>())
                .build();

        var technology = Technology.builder()
                                   .name(createProjectRequest.getTech())
                                   .description(createProjectRequest.getProjectDescription())
                                   .link(createProjectRequest.getTechLink())
                                   .build();

        projectRepository.save(project);
        technologyRepository.save(technology);

        var team = Team.builder()
                       .area(createProjectRequest.getTeamArea())
                       .name(createProjectRequest.getTeamName())
                       .room(createProjectRequest.getTeamRoom())
                       .technology(technology)
                       .project(project)
                       .build();

        teamRepository.save(team);

        project.getTeams().add(team);

        return project.getId();
    }
}
